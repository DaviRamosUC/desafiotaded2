package application;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import entities.Professor;

public class Program {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		List<Professor> professores = new ArrayList<>();
		Professor p = new Professor();
		
		char saida = 's';
		while (saida=='s') {
			System.out.print("Informe o nome do professor: ");
			String nome = sc.nextLine();
			p = new Professor(nome);
			p.incluir(professores, p);
			System.out.println("Deseja cadastrar um novo professor? s/n");
			saida=sc.nextLine().charAt(0);
		}
		
		System.out.println("O n�mero de professores �: "+ p.professorSize(professores));
		
//		String r;
//		if (p.consultar(professores, p)) {
//			r = "Existe";
//		}else {
//			r= "N�o existe";
//		}
//		Bloco de cima igual ao de baixo 
		
		String resposta = (p.consultar(professores, p)) ? "Existe" : "N�o existe";
		
		System.out.println("O professor: "+resposta);
		
		System.out.println("Removendo o �ltimo professor...");
		p.remover(professores, p);
		System.out.println("O n�mero de professores �: "+ p.professorSize(professores));

		System.out.println("Removendo todos os professores...");
		p.professorClear(professores);
		
		System.out.println("O n�mero de professores �: "+ p.professorSize(professores));
		
		sc.close();

	}

}
