package entities;

import java.util.List;

import services.ProfessoresUCSALService;

public class Professor implements ProfessoresUCSALService {

	private String nome;

	public Professor() {

	}

	public Professor(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public boolean consultar(List<Professor> professores, Professor professor) {
		return professores.contains(professor);
	}

	@Override
	public void incluir(List<Professor> professores, Professor professor) {
		professores.add(professor);

	}

	@Override
	public void remover(List<Professor> professores, Professor professor) {
		professores.remove(professor);

	}

	@Override
	public int professorSize(List<Professor> professores) {
		return professores.size();
	}

	@Override
	public void professorClear(List<Professor> professores) {
		professores.clear();
	}

}
