package services;

import java.util.List;

import entities.Professor;

public interface ProfessoresUCSALService {

	public boolean consultar(List<Professor> professores, Professor professor);

	public void incluir(List<Professor> professores, Professor professor);

	public void remover(List<Professor> professores, Professor professor);

	public int professorSize(List<Professor> professores);

	public void professorClear(List<Professor> professores);
}
